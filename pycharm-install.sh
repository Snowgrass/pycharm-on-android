export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true
echo "Install vncserver"
apt install tigervnc-standalone-server tigervnc-common tigervnc-xorg-extension -y
echo "Install light gui(fluxbox)"
apt install fluxbox -y

echo "Writing launch script"
echo "#! /bin/sh" > /etc/X11/Xvnc-session
echo "test x"$SHELL" = x"" && SHELL=startfluxbox" >> /etc/X11/Xvnc-session
echo "test x"$1" = x"" && set — default" >> /etc/X11/Xvnc-session
echo "vncconfig -iconic &" >> /etc/X11/Xvnc-session
echo ""$SHELL" -l «EOF" >> /etc/X11/Xvnc-session
echo "exec /etc/X11/Xsession "$@"" >> /etc/X11/Xvnc-session
echo "EOF"$@"" >> /etc/X11/Xvnc-session
echo "vncserver -kill $DISPLAY" >> /etc/X11/Xvnc-session

echo "Installing java"
apt install openjdk-17 -y


echo "Downloading and extractioning ide"
cd && rm pycharm-on-android-master.tar.gz && cd pycharm-on-android-master
tar -xvf pycharm-community.tar.gz && rm pycharm-community.tar.gz && cd

echo "Downloading python and pip"
apt install python3 -y && apt install python3-pip -y
echo "Writing launch script"
touch start-pycharm.sh
echo "vncserver -kill :*" > start-pycharm.sh
echo "LD_PRELOAD=/lib/aarch64-linux-gnu/libgcc_s.so.1 vncserver :1" >> start-pycharm.sh
echo "export DISPLAY=":1"" >> start-pycharm.sh
echo "pycharm-on-android-master/pycharm-community/bin/pycharm.sh" >> start-pycharm.sh
chmod ugo+x start-pycharm.sh

vncpasswd 
